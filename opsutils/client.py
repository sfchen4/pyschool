import psutil

def get_mem():
    mem_info = psutil.virtual_memory()
    mem_dic={
        'total':mem_info.total,
        'available':mem_info.available,
        'free':mem_info.free,
        'used':mem_info.used,
        'percent':mem_info.percent
    }
    return mem_dic['percent']
def get_cpu():
    # cpu = psutil.cpu_times_percent(interval=1.00,percpu=True)
    # print(cpu)
    cpu=psutil.cpu_percent(interval=1, percpu=True)
    sum=0
    for i in cpu:
        sum+=i
    return  sum/len(cpu)/100
mem_per=psutil.virtual_memory().percent/100
cpu_per=get_cpu()
info={'mem':mem_per,'cpu':cpu_per}

diskpartinfo=psutil.disk_partitions()
for i in diskpartinfo:
    if i.opts=='cdrom':
        continue
    mountname=i.mountpoint.strip('\\|:')
    disk_per=psutil.disk_usage(i.mountpoint).percent/100
    # print("%s盘使用率:\t%s" %(i.mountpoint.strip('\\|:'),psutil.disk_usage(i.mountpoint).percent/100))
    info[mountname]=disk_per
print(info)

