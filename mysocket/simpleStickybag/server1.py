import socket
import subprocess
import struct
server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host='127.0.0.1'
port=8080
server.bind((host,port))
server.listen(5)
conn,addr=server.accept()
while True:
    cmd=conn.recv(1024)

    print("exec %s;" %cmd.decode('gbk'))
    obj=subprocess.Popen(cmd.decode('gbk'),shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    stdout=obj.stdout.read()
    stderr=obj.stderr.read()
    totalsize=len(stdout)+len(stderr)
    header=struct.pack('q',totalsize)
    conn.send(header)
    conn.send(stdout)
    conn.send(stderr)