import socket
import struct
import json
import os

class MYTCPServer:
    socket_family=socket.AF_INET
    socket_type=socket.SOCK_STREAM
    coding='gbk'
    recv_package_size=8192
#   server_dir='file_upload'
    max_tcp_links=5

    def __init__(self,server_address):
        self.server_address=server_address
        self.socket=socket.socket(self.socket_family,self.socket_type)
        self.bind()
        self.active()
    def bind(self):
        try:
            self.socket.bind(self.server_address)
        except Exception:
            self.socket.close()
            raise
    def active(self):
        self.socket.listen(self.max_tcp_links)
    def get_request(self):
        #self.connect,self.clientaddr=self.socket.accept()
        return self.socket.accept()

    def put(self,head_dic):
        filename=head_dic['file_name']
        filesize=head_dic['filesize']
        recvsize=0
        with open(filename,'wb') as f:
            while recvsize < filesize:
                data=self.connect.recv(self.recv_package_size)
                f.write(data)
                recvsize+=len(data)
                print("recv:%s" %recvsize)

    def run(self):
        while True:
            self.connect,self.clientaddr=self.get_request()
            print('from client',self.clientaddr)
            while True:
                try: #为了防止客户端断开异常，导致程序终止。
                    head_struct=self.connect.recv(4)
                    head_len=struct.unpack('i',head_struct)[0]
                    head_json_bytes=self.connect.recv(head_len)
                    head_json=str(head_json_bytes,encoding=self.coding)
                    head_dic=json.loads(head_json)
                    cmd=head_dic['cmd']
                    if hasattr(self,cmd):
                        func=getattr(self,cmd)
                        func(head_dic)
                except Exception:
                    break
server=MYTCPServer(('127.0.0.1',8080))
server.run()