import socket
import struct
import json
import os
class MYTCPClient:
    socket_family=socket.AF_INET
    socket_type=socket.SOCK_STREAM
    recv_package_size=8192
    coding='gbk'
    def __init__(self,server_address):
        self.server_address=server_address
        self.socket=socket.socket(self.socket_family,self.socket_type)
        self.connect()
    def connect(self):
        try:
            self.socket.connect(self.server_address)
        except:
            self.socket.close()
            raise
    def run(self):
        while True:
            inp=input(">>:").strip()
            cmds=inp.split()

            head_dic={
                'cmd':cmds[0],
                'file_name':cmds[1],
                'filesize':os.path.getsize(cmds[1])
            }

            if hasattr(self,cmds[0]):
                func=getattr(self,cmds[0])
                func(head_dic)
    def put(self,head_dic):
        head_json=json.dumps(head_dic)
        head_json_bytes=bytes(head_json,encoding=self.coding)
        head_len=len(head_json_bytes)
        head_struct=struct.pack('i',head_len)
        self.socket.send(head_struct)
        self.socket.send(head_json_bytes)
        sendsize=0
        with open(head_dic['file_name'],'rb') as f:
            for line in f:
                self.socket.send(line)
                sendsize+=len(line)
                print("sendsize:%s" %sendsize)
            else:
                print("upload finished.")
client=MYTCPClient(('127.0.0.1',8080))
client.run()