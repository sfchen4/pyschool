import socket
import subprocess
import json
import struct
server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)#买手机
host='127.0.0.1'
port=8080
server.bind((host,port)) #绑号
server.listen(5)  #绑号的手机上电
while True:#循环等待客户端接入
    conn,addr=server.accept()  #等待拨入
    #循环接收客户端数据
    while True:
        cmd=conn.recv(1024)
        if not cmd: break
        print('cmd: %s' % str(cmd,encoding='gbk'))
        #执行cmd命令获取结果，cmd为str需要转换为bytes
        res=subprocess.Popen(cmd.decode('gbk'), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout=res.stdout.read()
        stderr=res.stderr.read()
        #包装结果大小
        header_dic={
            'total_size':len(stdout)+len(stderr)
        }
        print("待发送的数据长度:",header_dic['total_size'])

        header_json=json.dumps(header_dic)
        #1.发送headers的长度
        header_bytes=header_json.encode('gbk')
        header_len_zip=struct.pack('i', len(header_bytes))
        conn.send(header_len_zip)
        #2.发送headers
        conn.send(header_json.encode('gbk'))
        #3.发送其他内容
        conn.send(stdout)
        conn.send(stderr)
    conn.close()
