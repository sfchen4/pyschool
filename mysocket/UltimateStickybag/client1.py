import socket
import json
import struct
client=socket.socket(socket.AF_INET,socket.SOCK_STREAM) #下载网络拨号软件
host='127.0.0.1'
port=8080
client.connect((host,port)) #打电话

while True:
    cmd=input("pls insert your command:").strip()
    if not cmd:
        continue
    #接收的cmd为str，所以必须转换为bytes发送
    client.send(cmd.encode('gbk'))
    #接收的data为bytes，所以必须转换为字符串才能显示
    #接收headers的长度数据
    header_len_zip=client.recv(4)
    header_len=struct.unpack('i', header_len_zip)[0]
    #接收headers并转换为dic
    header_json=client.recv(header_len).decode('gbk')
    header_dic=json.loads(header_json)
    #取出data size，然后接收数据
    total_size=header_dic['total_size']
    print("接收到数据长度：",total_size)
    #接收数据
    data=b""
    recv_size=0
    while recv_size < total_size:#注意是<
        temp_data=client.recv(1024)
        recv_size+=len(temp_data)
        data+=temp_data
    print(data.decode('gbk'))