import socket
import os
import struct
import json
sharedir=r'D:\data\pyschool\mysocket\filetrans_Optimizedversion\server\share'

def get(conn,cmds):
    filename = sharedir + '\\' + cmds[1]
    filesize = os.path.getsize(filename)
    header = {
        'file_name': filename,
        'filesize': filesize,
        'md5': 'axxx'
    }
    headerjson = json.dumps(header)
    packed_header = struct.pack('i', len(headerjson))

    conn.send(packed_header)
    conn.send(bytes(headerjson, encoding='gbk'))
    with open(filename, 'rb') as f:
        for line in f:
            conn.send(line)
def put(conn,cmds):
    filename = cmds[1]
    packed_header = conn.recv(4)
    headersize = struct.unpack('i', packed_header)[0]  # 必须取元组第一个
    headerjson = str(conn.recv(headersize), encoding='gbk')
    header = json.loads(headerjson)
    filesize = header['filesize']
    recvsize = 0
    with open("newrecv" + filename, 'wb') as f:  # 必须在下面while之前，不然每次都会覆盖
        while recvsize < filesize:
            data = conn.recv(1024)
            recvsize += len(data)  # 必须加上size，不然一直卡住在收
            f.write(data)
def run():
    server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server.bind(('127.0.0.1',8080))
    server.listen(5)

    while True:
        conn,addr=server.accept()
        while True:
            inp=str(conn.recv(1024), encoding='gbk')
            if not inp:break
            cmds=inp.split()
            if cmds[0] == 'get':
                get(conn,cmds)
            elif cmds[0] == 'put':
                put(conn,cmds)
        conn.close()
if __name__ == '__main__':
    run()