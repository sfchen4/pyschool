import socket
import struct
import json

import os

downloaddir = r'D:\data\pyschool\mysocket\filetrans_Optimizedversion\client\download'

def get(client,cmds):
    filename = cmds[1]
    packed_header = client.recv(4)
    headersize = struct.unpack('i', packed_header)[0]  # 必须取元组第一个
    headerjson = str(client.recv(headersize), encoding='gbk')
    header = json.loads(headerjson)
    filesize = header['filesize']
    recvsize = 0
    with open(downloaddir + '\\' + "new" + filename, 'wb') as f:  # 必须在下面while之前，不然每次都会覆盖
        while recvsize < filesize:
            data = client.recv(1024)
            recvsize += len(data)  # 必须加上size，不然一直卡住在收
            f.write(data)
            print("totalsize:%s downloadsize:%s" % (filesize, recvsize))
def put(client,cmds):
    filename=cmds[1]
    filesize = os.path.getsize(filename)
    header = {
        'file_name': filename,
        'filesize': filesize,
        'md5': 'axxx'
    }
    headerjson = json.dumps(header)
    packed_header = struct.pack('i', len(headerjson))

    client.send(packed_header)
    client.send(bytes(headerjson, encoding='gbk'))
    sendsize=0
    with open(filename, 'rb') as f:
        for line in f:
            client.send(line)
            sendsize+=len(line)
            print("totalsize:%s sendsize:%s" %(filesize,sendsize))
def run():
    client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    client.connect(('127.0.0.1',8080))
    while True:
        inp=input('>:').strip()
        if not inp:continue
        client.send(bytes(inp, encoding='gbk'))
        cmds=inp.split()
        if cmds[0] == 'get':
            get(client,cmds)
        elif cmds[0] == 'put':
            put(client,cmds)
if __name__ == '__main__':
    run()