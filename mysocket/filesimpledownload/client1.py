import socket
import struct
import json
client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect(('127.0.0.1',8080))

cmd=input('>:').strip()
client.send(bytes(cmd,encoding='gbk'))
filename=cmd.split()[1]

packed_header=client.recv(4)
headersize=struct.unpack('i',packed_header)[0] #必须取元组第一个
headerjson=str(client.recv(headersize),encoding='gbk')
print(headerjson)
header=json.loads(headerjson)
filesize=header['filesize']
recvsize=0
with open("new"+filename,'wb') as f: #必须在下面while之前，不然每次都会覆盖
    while recvsize < filesize:
        data=client.recv(1024)
        recvsize+=len(data) #必须加上size，不然一直卡住在收
        f.write(data)