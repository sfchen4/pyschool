import socket
import os
import struct
import json
server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind(('127.0.0.1',8080))
server.listen(5)
conn,addr=server.accept()


cmd=str(conn.recv(1024),encoding='gbk')

filename=cmd.split()[1]
filesize=os.path.getsize(filename)
header={
    'file_name':filename,
    'filesize':filesize,
    'md5':'axxx'
}

headerjson=json.dumps(header)
packed_header=struct.pack('i',len(headerjson))

conn.send(packed_header)
conn.send(bytes(headerjson,encoding='gbk'))

#open file
with open(filename,'rb') as f:
    for line in f:
        conn.send(line)
