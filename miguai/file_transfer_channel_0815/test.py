# -*- coding:utf-8 -*-
import sys
import getopt

class TEST:
    def getargs(self):
        try:
            # --src=upload/1.jpg
            # --destaddr=10.0.0.20
            # --destport=21
            # --destuser=ftpuser
            # --destpassword=ftppass
            # --destdir=test
            opts, args = getopt.getopt(sys.argv[1:], "hs:a:P:u:p:d:",
                                       ["help", "src_path=", "dest_addr=", "dest_port=",
                                        "dest_user=", "dest_password=", "dest_dir="])
            # print(opts)
            for opt_name, opt_value in opts:
                if opt_name in ('-h', '--help'):
                    self.usage()
                if opt_name in ('-s', '--src_path'):
                    self.src_path = opt_value
                if opt_name in ('-a', '--dest_addr'):
                    self.dest_addr = opt_value
                if opt_name in ('-P', '--dest_port'):
                    self.dest_port=opt_value
                if opt_name in ('-u', '--dest_user'):
                    self.dest_name=opt_value
                if opt_name in ('-p', '--dest_password'):
                    self.dest_password=opt_value
                if opt_name in ('-d', '--dest_dir'):
                    self.dest_dir=opt_value
            print(self.src_path,self.dest_addr,self.dest_port,self.dest_name,self.dest_password,self.dest_dir)
        except getopt.GetoptError:
            self.usage()
            # print help information and exit:
    def usage(self):
        helpinfo='''
        usage:client.py 
        '''
        print(helpinfo)
TEST().getargs()
