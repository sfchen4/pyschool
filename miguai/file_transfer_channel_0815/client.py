#coding:utf-8
import socket
import struct
import json
import os
import sys
import configparser
import threading
import hashlib
import getopt

class MYTCPClient:
    address_family = socket.AF_INET
    socket_type = socket.SOCK_STREAM
    allow_reuse_address = False
    max_packet_size = 8192
    coding='utf-8'
    request_queue_size = 5
    def __init__(self,configfile,connect=True,**kwargs):
        self.configfile=configfile
        self.args_dic=self.get_config()
        self.src_path=self.args_dic['src_path']
        self.dmz_addr=self.args_dic['dmz_addr']
        self.dmz_port=self.args_dic['dmz_port']
        self.dmz_user=self.args_dic['dmz_user']
        self.dmz_pass=self.args_dic['dmz_pass']
        self.server_address=(self.dmz_addr,int(self.dmz_port))
        print("connect to",self.server_address)
        self.socket = socket.socket(self.address_family,self.socket_type)
        if connect:
            try:
                self.client_connect()
            except:
                self.client_close()
                raise
    def client_connect(self):
        self.socket.connect(self.server_address)
    def client_close(self):
        self.socket.close()
    def run(self):
        self.put()
    def put(self,*args):
        filename=self.src_path
        if not os.path.isfile(filename):
            print('file:%s is not exists' %filename)
            return
        else:
            filesize=os.path.getsize(filename)
            self.args_dic['filesize']=filesize
            self.args_dic['filename']=os.path.basename(filename)
        head_dic=self.args_dic
        head_json=json.dumps(head_dic)
        head_json_bytes=bytes(head_json,encoding=self.coding)
        head_struct=struct.pack('i',len(head_json_bytes))
        self.socket.send(head_struct)
        self.socket.send(head_json_bytes)
        send_size=0
        total_size=self.args_dic['filesize']
        print("uploading %s" % filename)
        try:
            with open(filename,'rb') as f:
                for line in f:
                    self.socket.send(line)
                    send_size+=len(line)
                    # print(send_size)
                    send_per = int(100 * send_size / total_size)
                    self.progress(send_per, width=30)
                else:
                    print()
                    print('upload successful')
        except:
            print("upload faild")
    def progress(self,percent, width=50):
        '''进度打印功能'''
        if percent >= 100:
            percent = 100
        show_str = ('[%%-%ds]' % width) % (int(width * percent / 100) * "#")  # 字符串拼接的嵌套使用
        print('\r%s %d%%' % (show_str, percent), end='')
    def get_config(self):
        '''获取配置文件信息'''
        if not os.path.exists(self.configfile):
            print("config file not exists! ")
        config=configparser.ConfigParser()
        config.read(self.configfile)
        src_path=config.get('client','src_path')
        dmz_addr = config.get('client', 'dmz_addr')
        dmz_port = config.get('client', 'dmz_port')
        dmz_user = config.get('client', 'dmz_user')
        dmz_pass = config.get('client', 'dmz_pass')
        dmz_dir = config.get('client', 'dmz_dir')
        dest_addr = config.get('client', 'dest_addr')
        dest_port = config.get('client', 'dest_port')
        dest_user = config.get('client', 'dest_user')
        dest_pass = config.get('client', 'dest_pass')
        dest_path = config.get('client', 'dest_path')
#
        # hash = hashlib.md5()
        # hash.update(dest_pass.encode('utf-8'))
        # hashpassword=hash.hexdigest()
        # print(type(hashpassword))
#
        config_dic = {'src_path': src_path,
                    'dmz_addr': dmz_addr,
                    'dmz_port': dmz_port,
                    'dmz_user': dmz_user,
                    'dmz_pass': dmz_pass,
                    'dmz_dir': dmz_dir,
                    'dest_addr': dest_addr,
                    'dest_port': dest_port,
                    'dest_user': dest_user,
                    'dest_pass': dest_pass,
                    'dest_path': dest_path}
        return config_dic
    def getargs(self):
        try:
            #--src=upload/1.jpg
            #--destaddr=10.0.0.20
            #--destport=21
            #--destuser=ftpuser
            #--destpassword=ftppass
            #--destdir=test
            opts, args = getopt.getopt(sys.argv[1:], "hs:a:P:u:p:d:",
                                       ["help","srcpath", "destaddr=","destport",
                                        "destuser","destpassword","destdir"])
        except getopt.GetoptError:
            pass
            # print help information and exit:

if __name__ == '__main__':
    client = MYTCPClient('client.conf',True)
    client.run()


