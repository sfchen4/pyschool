import socket
import struct
import json
import subprocess
import os
import configparser
import threading
import logging
from ftplib import FTP
import hashlib

class MyLogger():
    #scream handler
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    def __init__(self,name,logfile,level,scream_activate=False):
        self.name=name
        self.logfile=logfile
        self.level=level
        self.scream_activate=scream_activate
        self.filehandler = logging.FileHandler(logfile)
        self.filehandler.setFormatter(self.formatter)
        if self.scream_activate:
            self.handler=logging.StreamHandler()
            self.handler.setFormatter(self.formatter)

    def getMyLogger(self):
        logger = logging.getLogger(self.name)
        logger.setLevel(self.level)  # logger 优先级高于其它输出途径的
        # add handler to logger instance
        if self.scream_activate:
            logger.addHandler(self.handler)
        logger.addHandler(self.filehandler)
        return logger

class MyFtp():
    def __init__(self):
        self.ftp_client = FTP()
    def ftp_login(self,host_ip,host_port,username,password):
        '''
        连接ftp服务器并登陆
        :param host_ip:
        :param host_port:
        :param username:
        :param password:
        :return:
        '''
        try:
            self.ftp_client.connect(host_ip,int(host_port),timeout=10)
        except :
            logging.warning('network connect time out')
            return 1001
        try:
            self.ftp_client.login(user=username, passwd=password)
        except:
            logging.warning('username or password error')
            return 1002
        return 1000
    def execute_cmd(self,cmd):
        '''
        执行命令
        :param cmd:
        :return:
        '''
        command_result = self.ftp_client.sendcmd(cmd)
        logging.warning('command_result:%s'% command_result)
    def change_dir(self,dirname):
        try:
            res=self.ftp_client.cwd(dirname)
        except Exception:
            print("no dir named: %s start to make dir" %dirname)
            self.ftp_client.mkd(dirname)
            res = self.ftp_client.cwd(dirname)

    def list_dir(self):
        '''
        列表文件
        :return:
        '''
        command_result = self.ftp_client.pwd()
        logging.warning('command_result:%s' % command_result)
    def upload_file(self,filename):
        '''
        上传文件
        :param filename:
        :return:
        '''
        destfilename=os.path.basename(filename)
        print("destfilename:%s" % destfilename )
        command_result = self.ftp_client.storbinary('stor %s' %destfilename,open(filename,'rb'))
        logging.warning('command_result:%s' % command_result)
    def download_file(self,filename):
        '''
        下载文件
        :param filename:
        :return:
        '''
        command_result = self.ftp_client.retrbinary('retr %s' %filename, open(filename, 'wb').write)
        logging.warning('command_result:%s' % command_result)
    def ftp_logout(self):
        '''
        退出登陆
        :return:
        '''
        logging.warning('now will disconnect with ftp server')
        self.ftp_client.close()


class MYTCPServer:
    address_family = socket.AF_INET

    socket_type = socket.SOCK_STREAM

    allow_reuse_address = False

    max_packet_size = 8192

    coding='utf-8'

    request_queue_size = 100


    def __init__(self,configfile,bind_and_activate=True):
        """Constructor.  May be extended, do not override."""
        self.configfile = configfile
        self.get_config()
        # self.server_address=server_address
        # self.user=user
        # self.password=password

        self.socket = socket.socket(self.address_family,
                                    self.socket_type)
        if bind_and_activate:
            try:
                self.server_bind()
                self.server_activate()
            except:
                self.server_close()
                raise

    def server_bind(self):
        """Called by constructor to bind the socket.
        """
        if self.allow_reuse_address:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        print("bind ",self.server_address)
        self.socket.bind(self.server_address)
        self.server_address = self.socket.getsockname()

    def server_activate(self):
        """Called by constructor to activate the server.
        """
        self.socket.listen(self.request_queue_size)

    def server_close(self):
        """Called to clean-up the server.
        """
        self.socket.close()

    def get_request(self):
        """Get the request and client address from the socket.
        """
        return self.socket.accept()

    def close_request(self, request):
        """Called to clean up an individual request."""
        request.close()

    def run(self):
        while True:
            self.conn,self.client_addr=self.get_request()
            print('from client ',self.client_addr)
            while True:
                try:
                    head_struct = self.conn.recv(4)
                    if not head_struct:break

                    head_len = struct.unpack('i', head_struct)[0]
                    head_json = self.conn.recv(head_len).decode(self.coding)
                    head_dic = json.loads(head_json)
                    self.args_dic=head_dic
                    #判断head_dic的user和pass
                    # hash = hashlib.md5()
                    #
                    # hash.update(self.password)
                    #
                    # print(hash)
                    # hashpassword = hash.hexdigest()
                    # print(hashpassword)
                    if self.args_dic['dmz_user'] !=self.user or self.args_dic['dmz_pass'] !=self.password:
                        self.conn.close()
                        print("你的认证不通过")
                        break
                    self.put()
                except Exception:
                    break

    def put(self,*args):
        filename=self.args_dic['filename']
        server_dir=self.args_dic['dmz_dir']
        if not os.path.exists(server_dir):
            os.makedirs(server_dir)
        file_path=os.path.normpath(os.path.join(
            server_dir,
            filename
        ))

        filesize=self.args_dic['filesize']
        recv_size=0
        print('----->',file_path)
        # with open(file_path,'wb') as f:
        self.recv_status = False
        f=open(file_path,'wb')
        while recv_size < filesize:
            recv_data=self.conn.recv(self.max_packet_size)
            f.write(recv_data)
            recv_size+=len(recv_data)
            recv_per = int(100 * recv_size / filesize)
            self.progress(recv_per, width=30)
        else:
            print()
            self.recv_status=True
            f.close()
        if self.recv_status:
            self.sendbyftp(file_path)
    def sendbyftp(self,file_path):
        print("开始发送文件到外部网络，发送的方式是ftp")
        # filename='1.jpg'
        host_ip = self.args_dic['dest_addr']
        host_port = self.args_dic['dest_port']
        username = self.args_dic['dest_user']
        password = self.args_dic['dest_pass']
        dirname = self.args_dic['dest_path']

        ftpclient = MyFtp()
        print("开始连接远程主机%s:%s" %(host_ip,host_port))
        ftpclient.ftp_login(host_ip, host_port, username, password)
        ftpclient.change_dir(dirname)
        ftpclient.upload_file(file_path)
        ftpclient.ftp_logout()
    def progress(self,percent, width=50):
        '''进度打印功能'''
        if percent >= 100:
            percent = 100
        show_str = ('[%%-%ds]' % width) % (int(width * percent / 100) * "#")  # 字符串拼接的嵌套使用
        print('\r%s %d%%' % (show_str, percent), end='')

    def get_config(self):
        '''
        获取配置文件中的参数，赋值为对象的数据属性
        :return:
        '''
        if not os.path.exists(self.configfile):
            print("config file not exists! ")
            exit()
        config=configparser.ConfigParser()
        config.read(self.configfile)
        address = config.get('server', 'address')
        port = config.get('server', 'port')
        user = config.get('server', 'user')
        password=config.get('server', 'password')

        self.server_address = (address,int(port))
        self.user = user
        self.password = password

tcpserver1=MYTCPServer('server.conf')
tcpserver1.run()
