import getopt,sys,os
import configparser
import socket
import struct,json
import hashlib
#0、从配置文件 获取参数args:serverip,serverport,user,password

def getconfigfile():
    configfile=None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "c:i:",["help"])
        for opt_name, opt_value in opts:
            if opt_name == '--help':
                usage()
                exit()
            if opt_name == '-c':
                configfile = opt_value
    except getopt.GetoptError:
        usage()
        exit()
    if not os.path.exists(configfile):
        print("config file not exists! ")
        exit()
    return configfile
def usage():
    helpinfo='''
    Usage: %s [OPTION]...
    short option and long option can be used.
    all option must be contain.
    the information of option at bellow:

    --help  print the usage of the command
    -c      set the config file path  
    ''' %os.path.basename(sys.argv[0])
    print(helpinfo)
    exit()
def getconfigdic(configfile):
    configdic={}
    try:
        config = configparser.ConfigParser()
        config.read(configfile)
        configdic['serverip'] = config.get('server', 'serverip')
        configdic['serverport'] = config.get('server', 'serverport')
        configdic['user'] = hashauth(config.get('server', 'user'))
        configdic['password'] = hashauth(config.get('server', 'password'))
    except Exception as e:
        print("something go wrong:%s" %e)
        configdic = None
    return configdic
#1、通过参数建立socket
def getsocket(configdic):
    serverip=configdic['serverip']
    serverport=int(configdic['serverport'])
    server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server.bind((serverip,serverport))
    server.listen(100)
    return server
#2、等待客户端连接
def getconn(serversocket):
    conn,addr=serversocket.accept()
    return conn
def getheaddic(conn):
#3、接收客户端头数据中的json
    head_struct=conn.recv(4)
    head_len = struct.unpack('i', head_struct)[0]
    head_json = conn.recv(head_len).decode('utf-8')
    head_dic = json.loads(head_json)
    return head_dic
#5、认证,启动服务端，需要设置密码，将密码转换为md5值存入，然后接收客户端密码，md5比对
def authenticate(head_dic,configdic):
    status=False
    clientuser=head_dic['user']
    clientpassword=head_dic['password']
    serveruser=configdic['user']
    serverpassword=configdic['password']
    if clientuser == serveruser and clientpassword == serverpassword:
        status=True
    else:
        status=False
        print("authenticate faild")
        exit()
    return status
#6、接收客户端头数据中的json中指定size的文件
def getfile(head_dic,conn):
    filename=os.path.basename(head_dic['filepath'])
    remotedir=head_dic['remotedir']
    if not os.path.exists(remotedir):
        os.makedirs(remotedir)
    remotepath=os.path.normpath(os.path.join(remotedir,filename))
    filesize = head_dic['filesize']
    recv_size = 0

    f = open(remotepath+".new", 'wb')
    while recv_size < filesize:
        recv_data = conn.recv(8192)
        f.write(recv_data)
        recv_size += len(recv_data)
    else:
        f.close()
        if os.path.getsize(remotepath+".new") == filesize:
            print("接收成功")
            if os.path.exists(remotepath):
                if os.path.exists(remotepath+".old"):os.remove(remotepath+".old")
                os.rename(remotepath, remotepath+".old")
                os.rename(remotepath + ".new", remotepath)
        else:
            print("接收失败")
            os.remove(remotepath+".new")


#7、后续触发工作
def follow():
    pass
def hashauth(auth):
    m=hashlib.md5()
    m.update(auth.encode('utf-8'))
    hashstring=m.hexdigest()
    return hashstring
def run():
    configfile=getconfigfile()
    configdic=getconfigdic(configfile)
    if not configdic:
        exit()
    while True:
        serversocket=getsocket(configdic)
        while True:
            try:
                conn=getconn(serversocket)
                head_dic=getheaddic(conn)
                authenticate(head_dic, configdic)
                getfile(head_dic, conn)
                follow()
            except:
                continue
run()