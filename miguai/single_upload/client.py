#!/usr/bin/env python
#coding:utf-8
#author:chenshifei
#date:20180816
#info:upload file to server
#args:filepath serverip serverport user password remotedir otherargs
import socket
import getopt,sys,os
import json,struct
import hashlib
#1、创建socket
def getsocket(serverip,serverport):
    client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    serverport=int(serverport)
    client.connect((serverip,serverport))
    return client
#2、接收参数、前半部分必传选项、后半部分是额外可有可无的参数
def getargsdic():
    argsdic={}
    try:
        opts, args = getopt.getopt(sys.argv[1:], "f:h:P:u:p:d:",["help"])
        if len(opts) < 6 and len(opts) != 1:
            usage()
            exit()
        for opt_name, opt_value in opts:
            if opt_name == '--help':
                usage()
                exit()
            if opt_name == '-f':
                argsdic['filepath'] = opt_value
            if opt_name == '-h':
                argsdic['serverip'] = opt_value
            if opt_name == '-P':
                argsdic['serverport'] = opt_value
            if opt_name == '-u':
                argsdic['user'] = hashauth(opt_value)
            if opt_name == '-p':
                argsdic['password'] = hashauth(opt_value)
            if opt_name == '-d':
                argsdic['remotedir'] = opt_value
        if len(args) > 0:
            argsdic['others'] = args
    except getopt.GetoptError:
        usage()
        exit()
    return argsdic
def usage():
    helpinfo='''
    Usage: %s [OPTION]...
    short option and long option can be used.
    all option must be contain.
    the information of option at bellow:

    --help  print the usage of the command
    -f      set the source file path  
    -h      set destination server address
    -P      set destination server port
    -u      set destination server user
    -p      set destination server password
    -d      set destination server dir 
    ''' %os.path.basename(sys.argv[0])
    print(helpinfo)
#3、按规则制作头字典，并将pack头和json的bytes头，和argsdic存入列表返回
def makeheadlist(argsdic):
    argsdic['filesize']=os.path.getsize(argsdic['filepath'])
    head_json=json.dumps(argsdic)
    head_json_bytes = bytes(head_json, encoding='utf-8')
    head_struct = struct.pack('i', len(head_json_bytes))
    return [head_struct,head_json_bytes,argsdic]
#4、传入头和socket发送文件
def sendfile(headlist,clientsocket):
    head_struct=headlist[0]
    head_json_bytes=headlist[1]
    argsdic=headlist[2]
    send_size = 0
    total_size = argsdic['filesize']

    clientsocket.send(head_struct)
    clientsocket.send(head_json_bytes)
    print("send %s" % argsdic['filepath'])
    try:
        with open(argsdic['filepath'], 'rb') as f:
            for line in f:
                clientsocket.send(line)
                send_size += len(line)
                percent = int(100 * send_size / total_size)
                width = 30
                if percent >= 100:
                    percent = 100
                show_str = ('[%%-%ds]' % width) % (int(width * percent / 100) * "#")  # 字符串拼接的嵌套使用
                print('\r%s %d%%' % (show_str, percent), end='')
            else:
                print()
                print('send success')
                clientsocket.close()
    except Exception as e:
        print("send faild ", e)
        clientsocket.close()
def hashauth(auth):
    m=hashlib.md5()
    m.update(auth.encode('utf-8'))
    hashstring=m.hexdigest()
    return hashstring
def run():
    argsdic=getargsdic()
    serverip=argsdic['serverip']
    serverport=argsdic['serverport']
    client=getsocket(serverip, serverport)
    headlist=makeheadlist(argsdic)
    sendfile(headlist, client)
run()