import sys
import getopt
import os
class Test():
    def __init__(self, connect=True, **kwargs):
        # self.dmz_addr=0
        # self.dmz_port=0
        # self.dmz_user=0
        # self.dmz_pass=1
        # self.dest_addr=0
        # self.dest_user=1
        # self.dest_port=1
        # self.dest_pass=1
        # self.dest_dir=1
        # self.dmz_dir=1
        self.setargs()
    def setargs(self):
        try:
            opts, args = getopt.getopt(sys.argv[1:], "hs:a:P:u:p:d:",
                                       ["help", "src_path=", "dmz_addr=", "dmz_port=",
                                        "dmz_user=", "dmz_pass=", "dmz_dir=", "dest_addr=", "dest_port=",
                                        "dest_user=", "dest_pass=", "dest_dir="])
            for opt_name, opt_value in opts:
                if opt_name in ('-h', '--help'):
                    self.usage()
                if opt_name in ('-s', '--src_path'):
                    self.src_path = opt_value
                if opt_name == '--dmz_addr':
                    self.dmz_addr = opt_value
                if opt_name == '--dmz_port':
                    self.dmz_port = opt_value
                if opt_name == '--dmz_user':
                    self.dmz_user = opt_value
                if opt_name == '--dmz_pass':
                    self.dmz_pass = opt_value
                if opt_name == '--dmz_dir':
                    self.dmz_dir = opt_value
                if opt_name in ('-a', '--dest_addr'):
                    self.dest_addr = opt_value
                if opt_name in ('-P', '--dest_port'):
                    self.dest_port = opt_value
                if opt_name in ('-u', '--dest_user'):
                    self.dest_user = opt_value
                if opt_name in ('-p', '--dest_pass'):
                    self.dest_pass = opt_value
                if opt_name in ('-d', '--dest_dir'):
                    self.dest_dir = opt_value
            self.args_dic = {'src_path': self.src_path,
                                 'dmz_addr': self.dmz_addr,
                                 'dmz_port': self.dmz_port,
                                 'dmz_user': self.dmz_user,
                                 'dmz_pass': self.dmz_pass,
                                 'dmz_dir': self.dmz_dir,
                                 'dest_addr': self.dest_addr,
                                 'dest_port': self.dest_port,
                                 'dest_user': self.dest_user,
                                 'dest_pass': self.dest_pass,
                                 'dest_path': self.dest_dir}
            # print(self.args_dic)
        except getopt.GetoptError as e:
            self.usage()
            exit()
    def usage(self):
        helpinfo='''
        Usage: %s [OPTION]...
        short option and long option can be used.
        all option must be contain.
        the information of option at bellow:

        -h  --help          print the usage of the command
        -s  --src_path      set the source file path  
        -a  --dest_addr     set destination server address
        -P  --dest_port     set destination server port
        -u  --dest_user     set destination server user
        -p  --dest_pass     set destination server password
        -d  --dest_dir      set destination server dir
        --dmz_addr          set the socket address of dmz area Transfer station 
        --dmz_port          set the socket port of dmz area Transfer station 
        --dmz_user          set the socket user of dmz area Transfer station 
        --dmz_pass          set the socket password of dmz area Transfer station 
        --dmz_dir           set the socket file upload dir of dmz area Transfer station 
        ''' %os.path.basename(sys.argv[0])
        print(helpinfo)

a=Test()