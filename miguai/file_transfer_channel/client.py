#coding:utf-8
import socket
import struct
import json
import os
import sys
import configparser
import threading
import hashlib
import getopt

class MYTCPClient:
    address_family = socket.AF_INET
    socket_type = socket.SOCK_STREAM
    allow_reuse_address = False
    max_packet_size = 8192
    coding='utf-8'
    request_queue_size = 5
    def __init__(self,connect=True,**kwargs):
        print("set the args!!")
        self.setargs()
        self.server_address=(self.dmz_addr,int(self.dmz_port))
        print("connect to..",self.server_address)
        print("get the socket")
        self.socket = socket.socket(self.address_family,self.socket_type)
        if connect:
            try:
                self.client_connect()
            except:
                self.client_close()
                raise
    def client_connect(self):
        self.socket.connect(self.server_address)
    def client_close(self):
        self.socket.close()
    def isexist(self,filename):
        if not os.path.isfile(filename):
            print('the file:%s is not exist' %filename)
            exit()
        else:
            print("file exist")
    def sendsomething(self,filename,head_dic,mysocket):
        '''
        :param filename: filepath
        :param head_dic: contain field filesize
        :param mysocket: alread created socket
        :return:
        '''
        head_json = json.dumps(head_dic)
        head_json_bytes = bytes(head_json, encoding=self.coding)
        head_struct = struct.pack('i', len(head_json_bytes))
        mysocket.send(head_struct)
        mysocket.send(head_json_bytes)
        send_size=0
        total_size=head_dic['filesize']
        print("uploading %s" % filename)
        try:
            with open(filename,'rb') as f:
                for line in f:
                    mysocket.send(line)
                    send_size+=len(line)
                    percent = int(100 * send_size / total_size)
                    width=30
                    if percent >= 100:
                        percent = 100
                    show_str = ('[%%-%ds]' % width) % (int(width * percent / 100) * "#")  # 字符串拼接的嵌套使用
                    print('\r%s %d%%' % (show_str, percent), end='')
                else:
                    print()
                    print('upload success')
        except Exception as e:
            print("upload faild ",e)

    def run(self,*args):
        filename=self.src_path
        self.isexist(filename)
        self.args_dic['filesize']=os.path.getsize(filename)
        self.args_dic['filename']=os.path.basename(filename)
        head_dic = self.args_dic
        mysocket=self.socket
        #发送文件
        self.sendsomething(filename,head_dic, mysocket)

    def setargs(self):
        try:
            opts, args = getopt.getopt(sys.argv[1:], "hs:a:P:u:p:d:",
                                       ["help", "src_path=","dmz_addr=","dmz_port=",
                                        "dmz_user=","dmz_pass=","dmz_dir=","dest_addr=", "dest_port=",
                                        "dest_user=", "dest_pass=", "dest_dir="])
            for opt_name, opt_value in opts:
                if opt_name in ('-h', '--help'):
                    self.usage()
                if opt_name in ('-s', '--src_path'):
                    self.src_path = opt_value
                if opt_name == '--dmz_addr':
                    self.dmz_addr = opt_value
                if opt_name == '--dmz_port':
                    self.dmz_port = opt_value
                if opt_name == '--dmz_user':
                    self.dmz_user = opt_value
                if opt_name == '--dmz_pass':
                    self.dmz_pass = opt_value
                if opt_name == '--dmz_dir':
                    self.dmz_dir = opt_value
                if opt_name in ('-a', '--dest_addr'):
                    self.dest_addr = opt_value
                if opt_name in ('-P', '--dest_port'):
                    self.dest_port=opt_value
                if opt_name in ('-u', '--dest_user'):
                    self.dest_user=opt_value
                if opt_name in ('-p', '--dest_pass'):
                    self.dest_pass=opt_value
                if opt_name in ('-d', '--dest_dir'):
                    self.dest_dir=opt_value
                # else:
                #     self.usage()
            self.args_dic = {'src_path': self.src_path,
                              'dmz_addr': self.dmz_addr,
                              'dmz_port': self.dmz_port,
                              'dmz_user': self.dmz_user,
                              'dmz_pass': self.dmz_pass,
                              'dmz_dir': self.dmz_dir,
                              'dest_addr': self.dest_addr,
                              'dest_port': self.dest_port,
                              'dest_user': self.dest_user,
                              'dest_pass': self.dest_pass,
                              'dest_path': self.dest_dir}
        except getopt.GetoptError:
            self.usage()
            exit()
    def usage(self):
        helpinfo='''
        Usage: %s [OPTION]...
        short option and long option can be used.
        all option must be contain.
        the information of option at bellow:

        -h  --help          print the usage of the command
        -s  --src_path      set the source file path  
        -a  --dest_addr     set destination server address
        -P  --dest_port     set destination server port
        -u  --dest_user     set destination server user
        -p  --dest_pass     set destination server password
        -d  --dest_dir      set destination server dir
        --dmz_addr          set the socket address of dmz area Transfer station 
        --dmz_port          set the socket port of dmz area Transfer station 
        --dmz_user          set the socket user of dmz area Transfer station 
        --dmz_pass          set the socket password of dmz area Transfer station 
        --dmz_dir           set the socket file upload dir of dmz area Transfer station 
        ''' %os.path.basename(sys.argv[0])
        print(helpinfo)
if __name__ == '__main__':
    client = MYTCPClient(True)
    client.run()