import socket
import struct
import json
import subprocess
import os
import configparser

class MYTCPServer:
    address_family = socket.AF_INET

    socket_type = socket.SOCK_STREAM

    allow_reuse_address = False

    max_packet_size = 8192

    coding='utf-8'

    request_queue_size = 100


    def __init__(self,configfile,server_address,user,password,bind_and_activate=True):
        """Constructor.  May be extended, do not override."""
        self.server_address=server_address
        self.user=user
        self.password=password
        self.configfile=configfile
        # self.get_config()
        self.socket = socket.socket(self.address_family,
                                    self.socket_type)
        if bind_and_activate:
            try:
                self.server_bind()
                self.server_activate()
            except:
                self.server_close()
                raise

    def server_bind(self):
        """Called by constructor to bind the socket.
        """
        if self.allow_reuse_address:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        print("bind ",self.server_address)
        self.socket.bind(self.server_address)
        self.server_address = self.socket.getsockname()

    def server_activate(self):
        """Called by constructor to activate the server.
        """
        self.socket.listen(self.request_queue_size)

    def server_close(self):
        """Called to clean-up the server.
        """
        self.socket.close()

    def get_request(self):
        """Get the request and client address from the socket.
        """
        return self.socket.accept()

    def close_request(self, request):
        """Called to clean up an individual request."""
        request.close()

    def run(self):
        while True:
            self.conn,self.client_addr=self.get_request()
            print('from client ',self.client_addr)
            while True:
                try:
                    head_struct = self.conn.recv(4)
                    if not head_struct:break

                    head_len = struct.unpack('i', head_struct)[0]
                    head_json = self.conn.recv(head_len).decode(self.coding)
                    head_dic = json.loads(head_json)
                    self.args_dic=head_dic
                    #判断head_dic的user和pass
                    if self.args_dic['dmz_user'] !=self.user or self.args_dic['dmz_pass'] !=self.password:
                        self.conn.close()
                        print("你的认证不通过")
                        break
                    # print(self.args_dic)
                    #head_dic={'cmd':'put','filename':'a.txt','filesize':123123}
                #     cmd=head_dic['cmd']
                #     if hasattr(self,cmd):
                #         func=getattr(self,cmd)
                #         func(head_dic)
                    self.put()
                except Exception:
                    break

    def put(self,*args):
        filename=self.args_dic['filename']
        server_dir=self.args_dic['dmz_dir']
        if not os.path.exists(server_dir):
            os.makedirs(server_dir)
        file_path=os.path.normpath(os.path.join(
            server_dir,
            filename
        ))

        filesize=self.args_dic['filesize']
        recv_size=0
        print('----->',file_path)
        # with open(file_path,'wb') as f:
        f=open(file_path,'wb')
        while recv_size < filesize:
            recv_data=self.conn.recv(self.max_packet_size)
            f.write(recv_data)
            recv_size+=len(recv_data)
            # print('recvsize:%s filesize:%s' %(recv_size,filesize))
            # print("#%.2f%%" % (recv_size / filesize * 100))
            # import time
            # time.sleep(0.5)
            recv_per = int(100 * recv_size / filesize)
            self.progress(recv_per, width=30)
        else:
            print()
            print("开始发送文件到外部网络，听说要经过F5，发送的方式是ftp")
            f.close()
    def progress(self,percent, width=50):
        '''进度打印功能'''
        if percent >= 100:
            percent = 100
        show_str = ('[%%-%ds]' % width) % (int(width * percent / 100) * "#")  # 字符串拼接的嵌套使用
        print('\r%s %d%%' % (show_str, percent), end='')
    def get_config(self):
        if not os.path.exists(self.configfile):
            print("config file not exists! ")
        config=configparser.ConfigParser()
        config.read(self.configfile)
        address = config.get('server', 'address')
        port = config.get('server', 'port')
        user = config.get('server', 'user')
        password=config.get('server', 'password')
        self.server_address = address
        self.user = user
        self.password = password
tcpserver1=MYTCPServer('client.conf',('0.0.0.0',8888),'dmzuser','dmzpass')

tcpserver1.run()