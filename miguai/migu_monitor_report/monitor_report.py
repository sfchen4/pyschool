#!/usr/bin/env python2.7
# coding:utf-8
#capacity_statistics.py
import urllib2
import json
import time
import sys
import os
import re
import ConfigParser
class MonitorReport(object):
    headers={'Content-Type': 'application/json'}

    def __init__(self,url,log_name,log_type,name_dict,param_state=True):
        self.url=url
        self.name_dict = name_dict
        if param_state:
            #控制是否接收系统传参
            log_args=self.getargs()
            self.log_name=log_args[0]
            self.log_type=log_args[1]
        else:
            self.log_name=log_name
            self.log_type=log_type
    #通用函数
    def send_data(self,data):
        try:
            request = urllib2.Request(url=self.url, headers=self.headers,data=json.dumps(data))
            response = urllib2.urlopen(request,timeout=2)
            print(response.read())
        except urllib2.URLError as e:
            print("发送数据失败，原因是url不可达")
            print(data)
            exit()
        except Exception as e:
            print("发送数据失败,exp: %s" %e)
            exit()
    #需要按自己的规则解析，可修改的函数
    def content_parser(self):
        content_dict={}
        # {'20180730': {'car_re': {'ability_calls': 1, 'ability_rate': 1.0}}
        # {'20180730': {'cpumem': {'cpu_avg': 1.64, 'mem_avg': 9.03}}
        # {'20180730': {'gpu': {'gpu_avg': 1.64}}
        #可以在这里判断日志的类型，然后根据日志不同类型设置标志，进行不同的解析操作
        if self.log_type == "ability":
            with open(self.log_name,'r') as f:
                for line in f:
                    time_day,ability_name,ability_calls,ability_rate=re.split('[| ]+',line)
                    if time_day not in content_dict:
                        content_dict[time_day]={}
                    content_dict[time_day][ability_name]={'ability_calls':int(ability_calls),'ability_rate':float(ability_rate)}
        elif self.log_type == "cpumem":
            with open(self.log_name,'r') as f:
                content_dict = {}
                for line in f:
                    ex1, time_day, ex2, cpu_avg, ex3, mem_avg = re.split('[: ]+', line)
                    if time_day not in content_dict:
                        content_dict[time_day]={}
                    content_dict[time_day]['cpumem']={'cpu_avg':float(cpu_avg),'mem_avg':float(mem_avg)}
        elif self.log_type == "gpu":
            with open(self.log_name,'r') as f:
                for line in f:
                    ex1, time_day, ex2, gpu_avg = re.split('[: ]+', line)
                    if time_day not in content_dict:
                        content_dict[time_day]={}
                    content_dict[time_day]['gpu']={'gpu_avg':float(gpu_avg)}
        else:
            print("unkown log type")
            exit()
        return content_dict
    #在这里修改json格式
    def get_data(self,curr_minutes,time_day,business_name,values):
        data={}
        business_seconds=time_day+"2359"
        if self.log_type == "ability":
            data={"SysCode": "3023", "Time": curr_minutes, "SerialNumber": 110,
                          "Data": [{"JCZB_Code": "302301610300",
                                    "Aggregate_Flag": 0,
                                    "Log_Time": curr_minutes,
                                    "JCZB_Dim_Value": "10:"+business_seconds+";185:"+business_name,
                                    "JCZB_Value": values[0]},
                                   {"JCZB_Code": "302301610000",
                                    "Aggregate_Flag": 0,
                                    "Log_Time": curr_minutes,
                                    "JCZB_Dim_Value": "10:"+business_seconds+";184:"+business_name,
                                    "JCZB_Value": values[1]}
                                   ]}
        elif self.log_type == "cpumem":
            data={"SysCode":"3023","Time":curr_minutes,"SerialNumber":110,
                      "Data":[{"JCZB_Code":"302301612000",
                               "Aggregate_Flag":0,
                               "Log_Time":curr_minutes,
                               "JCZB_Dim_Value":"10:"+business_seconds+";200:"+business_name,
                               "JCZB_Value":values[0]
                               },
                              {"JCZB_Code":"302301612100",
                               "Aggregate_Flag":0,
                               "Log_Time":curr_minutes,
                               "JCZB_Dim_Value":"10:"+business_seconds+";200:"+business_name,
                               "JCZB_Value":values[1]
                               }
                              ]}
        elif self.log_type == "gpu":
            data={"SysCode":"3023",
                  "Time":curr_minutes,
                  "SerialNumber":110,
                  "Data":[{"JCZB_Code":"302301612200",
                           "Aggregate_Flag":0,
                           "Log_Time":curr_minutes,
                           "JCZB_Dim_Value":"10:"+business_seconds+";200:"+business_name,
                           "JCZB_Value":values[0]}]}
        else:
            print("unkown log type")
            exit()
        return data
    def set_config(self):
        pass
    def getargs(self):
        if len(sys.argv) < 3:
            print("3 args must be give,you have gived %s args" % len(sys.argv))
            exit()
        log_name = sys.argv[1]
        log_type = sys.argv[2]
        if not os.path.isfile(log_name):
            print("the log file %s does not exists!" % log_name)
            exit()
        else:
            if log_type not in ['cpumem', 'ability', 'gpu']:
                print("the type value must be in [cpumem,ability,gpu]")
                exit()
            else:
                return [log_name, log_type]
    def run(self):
        print("你发送的数据类型为：%s" %self.log_type)
        content_dict=self.content_parser()
        # print(content_dict)
        curr_minutes = time.strftime('%Y%m%d%H%M%S')
        # {'20180730': {'car_re': {'ability_calls': 1, 'ability_rate': 1.0}}
        # {'20180730': {'cpumem': {'cpu_avg': 1.64, 'mem_avg': 9.03}}
        # {'20180730': {'gpu': {'gpu_avg': 1.64}}
        for time_day in content_dict:
            inner_dict=content_dict[time_day]

            for business in inner_dict:
                business_name_cn=self.name_dict[business]
                values = []
                for option_name in inner_dict[business]:
                    values.append(inner_dict[business][option_name])
                data=self.get_data(curr_minutes,time_day,business_name_cn,values)
                self.send_data(data)

if __name__ == '__main__':
    name_dict = {
        'musicGene_re': '音乐情感基因识别',
        'car_re': '点播汽车识别',
        'adv_re': '直播广告识别',
        'star_re': '点播明星识别',
        'car_star_adv_re': '明星汽车广告识别',
        'Direct_seeding_emotion_recognition': '直播情绪识别',
        'Understanding_of_movement_posture': '运动姿势理解',
        'Good_recognition_and_understanding_of_games': '游戏精彩识别理解',
        'Google_Search_by_Image': '以图搜图',
        'Musical_emotion_gene_recognition': '音乐情感基因识别',
        'Face_recognition_of_immico': '咪咕人脸识别',
        'under_mov_post':'运动姿势理解',
        'gameai_re':'游戏精彩识别理解',
        'search_by_image':'以图搜图',
        'cpumem': '成都主节点',
        'gpu':'成都主节点',
        'huipai':'绘拍',
        'face':'咪咕人脸识别'
    }
    url='http://172.20.15.2:8081/ums/business/json/'
    log_file='ability.log'
    log_type='ability'
    # reportab = MonitorReport('http://172.20.15.2:8081/ums/business/json/', 'ability.log','ability',name_dict)
    # reportab.run()
    # reportgpu = MonitorReport('http://172.20.15.2:8081/ums/business/json/', 'gpu.log','gpu',name_dict)
    # reportgpu.run()
    # reportcpumem = MonitorReport('http://172.20.15.2:8081/ums/business/json/', 'cpumem.log','cpumem',name_dict)
    # reportcpumem.run()
    reportab = MonitorReport(url,log_file,log_type,name_dict,param_state=True)
    reportab.run()
