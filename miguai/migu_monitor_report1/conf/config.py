# coding:utf-8
import time
node_name='咪咕音乐'
base_path='/home/aicount/'
code_capnoc = "302301610000"
code_caprat = "302301610300"
code_cpu = "302301612000"
code_mem = "302301612100"
code_gpu = "302301612200"
serialnumber_cap=110
serialnumber_cpumem=110
serialnumber_gpu=401
Time=time.strftime('%Y%m%d%H%M%S')
Log_Time=Time


url = 'http://172.20.15.2:8081/ums/business/json/'
name_dict = {
    'musicGene_re': '音乐情感基因识别',
    'car_star_adv_re': '明星汽车广告识别',
    'Direct_seeding_emotion_recognition': '直播情绪识别',
    'Understanding_of_movement_posture': '运动姿势理解',
    'Good_recognition_and_understanding_of_games': '游戏精彩识别理解',
    'Google_Search_by_Image': '以图搜图',
    'Musical_emotion_gene_recognition': '音乐情感基因识别',
    'Face_recognition_of_immico': '咪咕人脸识别',
    'under_mov_post': '运动姿势理解',
    'gameai_re': '游戏精彩识别理解',
    'search_by_image': '以图搜图',
    'CPU_average': 'CPU平均使用率',
    'MEM_average': '内存平均使用率',
    'gpu_average': 'GPU平均使用率'
}
jczb_dim_value=None
JCZB_Value_capnoc=None
JCZB_Value_caprat=None


data_cap = {"SysCode": "3023", "Time": Time, "SerialNumber": 110,
        "Data": [
            {"JCZB_Code": "302301610000", "Aggregate_Flag": 0, "Log_Time": Log_Time,
             "JCZB_Dim_Value": jczb_dim_value, "JCZB_Value": JCZB_Value_capnoc},

            {"JCZB_Code": "302301610300", "Aggregate_Flag": 0, "Log_Time": Log_Time,
             "JCZB_Dim_Value": jczb_dim_value, "JCZB_Value": JCZB_Value_caprat}
        ]}



