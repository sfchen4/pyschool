#!/bin/bash
#0 12 */1 * * /bin/bash /home/aicount/shell/merg.sh &>/tmp/merg.log

merg_area='musicGene'

merg_name='musicGene_re'

txtdir=/home/aicount
#shell dir
shelldir=/home/aicount/shell

tempfile=/home/aicount/shell/sum.temp

yes_time=`date +%Y%m%d -d "1 day ago"`

sumfile='/home/aicount/shell/sum.log'

mergfile()
{
	txtdir=$1
	tempfile=$2
	truncate -s 0 $tempfile
	for i in ${txtdir}/*.txt
	do
	   cat $i>>$tempfile
	   echo ''>>$tempfile
	done
	sed -i /^$/d $tempfile
}


calfunc(){
	sumfile=$1
	merg_area=$2
	tempfile=$3
	yes_time=$4
	merg_name=$5
	truncate -s 0 $sumfile
	egrep $merg_area $tempfile|grep $yes_time|awk -v name=$merg_name -F '|' 'BEGIN{num_cal=0;suc_rat=0}{num_cal+=$3;suc_rat+=$4}END{print $1"|"name"|"num_cal"|"suc_rat}' >> $sumfile
	egrep -v $merg_area $tempfile|grep $yes_time>>$sumfile
}
mergfile $txtdir $tempfile
calfunc $sumfile $merg_area $tempfile $yes_time $merg_name

python ${shelldir}/merg_upload.py $sumfile capnum