#!/usr/bin/env python2.7
# coding:utf-8
#capacity_statistics.py
import urllib2
import json
import time
import sys
import os
def getargs():
    if len(sys.argv) < 3:
        print("3 args must be give,you have gived %s args" %len(sys.argv))
        exit()
    filename=sys.argv[1]
    jsontype=sys.argv[2]
    if not os.path.isfile(filename):
        print("the file %s does not exists!" %filename)
        exit()
    else:
        if jsontype not in ['cpumem', 'capnum','gpu']:
            print("the type value must be in [cpumem,capnum,gpu]")
            exit()
        else:
            return [filename,jsontype]
def sendjson(url,data):
    headers = {'Content-Type': 'application/json'}
    request = urllib2.Request(url=url, headers=headers, data=json.dumps(data))
    response = urllib2.urlopen(request)
    print(response.read())

def analysis_cap(filename):
    content_dict={}
    with open(filename,'r') as f:
        for line in f:
            # key,name,num_cal,suc_rate=line.split('|')
            line_list=line.split('|')
            key=line_list[0].strip()
            name=line_list[1].strip()
            num_cal=line_list[2].strip()
            suc_rate=line_list[3].strip()

            if key not in content_dict:
                content_dict[key]=[]
            inner_dic={}
            inner_dic['name']=name
            inner_dic['num_cal']=int(num_cal)
            inner_dic['suc_rate']=float(suc_rate)
            content_dict[key].append(inner_dic)
    return content_dict

def analysis_cpumem(filename):
    logfile=filename
    #logfile='/disk1/cpumemused/tmp/hy_average.log'
    content_dict={}
    with open(logfile,'r') as f:
        for line in f:
            loglist=line.split()
            # print(loglist)
            key=loglist[0].strip().split(':')[1]
            CPU_average=loglist[1].strip().split(':')[1]
            MEM_average=loglist[2].strip().split(':')[1]
            content_dict[key]=[CPU_average, MEM_average]
    return content_dict

def analysis_gpu(filename):
    logfile=filename
    content_dict={}
    with open(logfile,'r') as f:
        for line in f:
            loglist=line.split()
            # print(loglist)
            key=loglist[0].strip().split(':')[1]
            gpu_average=loglist[1].strip().split(':')[1]
            content_dict[key]=[gpu_average]
    return content_dict

def send_cap(content_dict):
    cur_time = time.strftime('%Y%m%d%H%M%S')
    # define data vars
    syscode = "3023"
    serialnumber = 110
    jczb_code_num = "302301610000"
    jczb_code_rate = "302301610300"
    aggregate_flag = 0
    cnlogo_num = 184
    cnlogo_rate = 185

    for business_date in content_dict:
        business_day=business_date+"2359"
        for info_dic in content_dict[business_date]:
            en_name=info_dic['name']
            #get value of the data
            number_of_calls = info_dic['num_cal']
            success_rate=info_dic['suc_rate']
            business_name = name_dict[en_name]

            jczb_dim_value_num="10:%s;%s:%s" %(business_day,cnlogo_num,business_name)
            jczb_dim_value_rate="10:%s;%s:%s" %(business_day,cnlogo_rate,business_name)

            data={"SysCode":syscode,"Time":cur_time,"SerialNumber":serialnumber,"Data":[

            {"JCZB_Code":jczb_code_num,
            "Aggregate_Flag":0,"Log_Time":cur_time,"JCZB_Dim_Value":jczb_dim_value_num,"JCZB_Value":number_of_calls}
            ,
            {"JCZB_Code":jczb_code_rate,
            "Aggregate_Flag":0,"Log_Time":cur_time,"JCZB_Dim_Value":jczb_dim_value_rate,"JCZB_Value":success_rate}

            ]}
            sendjson(url, data)
def sendcpumem(content_dict):
    current_time_min = time.strftime('%Y%m%d%H%M')
    current_time_sec = time.strftime('%Y%m%d%H%M') + "00"
    SerialNumber=110
    business_name_cpu = name_dict['CPU_average']
    business_name_mem = name_dict['MEM_average']
    JCZB_Code_cpu = '302301612000'
    JCZB_Code_mem = '302301612100'

    for business_date in content_dict:
        business_day=business_date+"2359"
        info_list = content_dict[business_date]
        CPU_average=info_list[0]
        MEM_average=info_list[1]

        data = {"SysCode":"3023",
        "Time":current_time_sec,
        "SerialNumber":SerialNumber,
        "Data":[
        {"JCZB_Code":JCZB_Code_cpu,
        "Aggregate_Flag":0,
        "Log_Time":current_time_min,
        "JCZB_Dim_Value":"10:"+business_day+";200:"+business_name_cpu,
        "JCZB_Value":CPU_average}
        ,
        {"JCZB_Code":JCZB_Code_mem,
        "Aggregate_Flag":0,
        "Log_Time":current_time_min,
        "JCZB_Dim_Value":"10:"+business_day+";200:"+business_name_mem,
        "JCZB_Value":MEM_average}
        ]
        }

        sendjson(url,data)

def sendgpu(content_dict):
    current_time_min = time.strftime('%Y%m%d%H%M')
    current_time_sec = time.strftime('%Y%m%d%H%M') + "00"
    SerialNumber=401
    business_name_gpu = name_dict['gpu_average']
    JCZB_Code_gpu = '302301612200'
    for business_date in content_dict:
        business_day=business_date+"2359"
        info_list = content_dict[business_date]
        gpu_average=info_list[0]
        data = {"SysCode":"3023",
        "Time":current_time_sec,
        "SerialNumber":SerialNumber,
        "Data":[
        {"JCZB_Code":JCZB_Code_gpu,
        "Aggregate_Flag":0,
        "Log_Time":current_time_min,
        "JCZB_Dim_Value":"10:"+business_day+";200:"+business_name_gpu,
        "JCZB_Value":gpu_average}
        ]
        }
        print(data)
        sendjson(url,data)
if __name__ == '__main__':
    name_dict = {
        'musicGene_re': '音乐情感基因识别',
        'car_re': '点播汽车识别',
        'adv_re': '直播广告识别',
        'star_re': '点播明星识别',
        'car_star_adv_re': '明星汽车广告识别',
        'Direct_seeding_emotion_recognition': '直播情绪识别',
        'Understanding_of_movement_posture': '运动姿势理解',
        'Good_recognition_and_understanding_of_games': '游戏精彩识别理解',
        'Google_Search_by_Image': '以图搜图',
        'Musical_emotion_gene_recognition': '音乐情感基因识别',
        'Face_recognition_of_immico': '咪咕人脸识别',
        'under_mov_post':'运动姿势理解',
        'gameai_re':'游戏精彩识别理解',
        'search_by_image':'以图搜图',
        'CPU_average': '成都主节点',
        'MEM_average': '成都主节点',
        'gpu_average':'成都主节点',
        'huipai':'绘拍',
        'face':'咪咕人脸识别'
    }
    url = 'http://172.20.15.2:8081/ums/business/json/'
    filename=getargs()[0]
    jsontype=getargs()[1]
    if jsontype == 'cpumem':
        content_dict=analysis_cpumem(filename)
        sendcpumem(content_dict)
    elif jsontype == 'capnum':
        content_dict=analysis_cap(filename)
        send_cap(content_dict)
    elif jsontype == 'gpu':
        content_dict=analysis_gpu(filename)
        sendgpu(content_dict)