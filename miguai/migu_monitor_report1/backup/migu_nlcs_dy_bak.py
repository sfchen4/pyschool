#!/usr/bin/env python2.7
# coding:utf-8
# import urllib2
import json
import time

filename='sum.txt' #sum.txt请用脚本将所有能力厂商数据汇总到一个文件中，并按日期进行切分。
'''
视讯: sx_20180730.txt #视讯服务器所有文件汇总0730号的数据到此文件
动漫: dm_20180730.txt #动漫服务器所有文件汇总0730号的数据到此文件
然后合并sx_20180730.txt和dm_20180730.txt为sum.txt
'''
# url='http://211.139.191.160:8081/ums/business/json/'
url = 'http://172.20.15.2:8081/ums/business/json/'
'''
filename内容格式如下:
20180729|car_re|0|0.000
20180729|star_re|0|0.000
20180729|adv_re|0|0.000
20180730|car_re|0|0.000
20180730|star_re|0|0.000
20180730|adv_re|0|0.000
'''

name_dict={
    'musicGene_re':'音乐情感基因识别',
    'car_re':'点播汽车识别',
    'adv_re':'直播广告识别',
    'star_re':'点播明星识别',
    'car_star_adv_re':'明星汽车广告识别',
    'Direct_seeding_emotion_recognition':'直播情绪识别',
    'Understanding_of_movement_posture':'运动姿势理解',
    'Good_recognition_and_understanding_of_games':'游戏精彩识别理解',
    'Google_Search_by_Image':'以图搜图',
    'Musical_emotion_gene_recognition':'音乐情感基因识别',
    'Face_recognition_of_immico':'咪咕人脸识别'
}

def analysis(filename):
    content_dict={}
    with open(filename,'r') as f:
        for line in f:
            line_list=line.split('|')
            key=line_list[0].strip()
            name=line_list[1].strip()
            num_cal=line_list[2].strip()
            suc_rate=line_list[3].strip()
            if key not in content_dict:
                content_dict[key]=[]
            inner_dic={}
            inner_dic['name']=name
            inner_dic['num_cal']=int(num_cal)
            inner_dic['suc_rate']=float(suc_rate)
            content_dict[key].append(inner_dic)
    return content_dict

'''
a={'20180729':[{'name':'star_re','num_cal':0,'suc_rate':0.00},
 'name':'car_re','num_cal':0,'suc_rate':0.00},
 {'name':'adv_re','num_cal':0,'suc_rate':0.00}]
 }
'''
# d={'20180729':{a['20180729'][0]['name']+a['20180729'][1]['name']+a['20180729'][2]['name'],
#                a['20180729'][0]['num_cal'] + a['20180729'][1]['num_cal'] + a['20180729'][2]['num_cal'],
#                a['20180729'][0]['suc_rate'] + a['20180729'][1]['suc_rate'] + a['20180729'][2]['suc_rate']}}
# name=''
# num=0
# rate=0.0
# for i in a[20180729]:
#     name=name+i['name']
#     num+=i['num_cal']
#     rate+=i['suc_rate']
# r={'20180729':{'name':name,'num_cal':num,'suc_rate':rate}}
# print(d)

res=analysis(filename)
# print(res)
#
def run(res):
    for business_date in res:
        current_time_min=time.strftime('%Y%m%d%H%M')
        current_time_sec=time.strftime('%Y%m%d%H%M')+"00"
        business_day=business_date+"2359"
        print(current_time_min,current_time_sec,business_day,'---------------------------------')
        for info_dic in res[business_date]:
            en_name=info_dic['name']
            business_name=name_dict[en_name]
            # print(business_name)
            number_of_calls = info_dic['num_cal']
            Success_rate=info_dic['suc_rate']
            JCZB_Type='302301MIGUAI01000001'
            data = {"SysCode":"3023",
            "Time":current_time_sec,
            "SerialNumber":401,
            "Data":[
            {"JCZB_Code":JCZB_Type,
            "Aggregate_Flag":0,
            "Log_Time":current_time_min,
            "JCZB_Dim_Value":"10:"+business_day+";70:"+business_name,
            "JCZB_Value":number_of_calls}
            ]
            }
            headers = {'Content-Type': 'application/json'}
            print('开始send数据:',data)
        # request = urllib2.Request(url=url, headers=headers, data=json.dumps(data))
        # response = urllib2.urlopen(request)
        # print(response)
run(res)
def run2():
    current_time_min=time.strftime('%Y%m%d%H%M')
    current_time_sec=time.strftime('%Y%m%d%H%M')+"00"
    business_day="201807262359"
    print(current_time_min,current_time_sec,business_day)
    business_name='音乐情感基因识别'

    JCZB_Type='302301MIGUAI01000001'
    number_of_calls=887
    Success_rate=100

    data = {"SysCode":"3023",
    "Time":current_time_sec,
    "SerialNumber":401,
    "Data":[
    {"JCZB_Code":JCZB_Type,
    "Aggregate_Flag":0,
    "Log_Time":current_time_min,
    "JCZB_Dim_Value":"10:"+business_day+";70:"+business_name,
    "JCZB_Value":number_of_calls}
    ]
    }
    headers = {'Content-Type': 'application/json'}
    # url='http://211.139.191.160:8081/ums/business/json/'
    url='http://172.20.15.2:8081/ums/business/json/'
    request = urllib2.Request(url=url, headers=headers, data=json.dumps(data))
    response = urllib2.urlopen(request)
    print(response)