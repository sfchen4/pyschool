#!/usr/bin/env python2.7
# coding:utf-8
#capacity_statistics.py
import urllib2
import json
import time
import sys
import os
name_dict = {
    'musicGene_re': '音乐情感基因识别',
    'car_star_adv_re': '明星汽车广告识别',
    'Direct_seeding_emotion_recognition': '直播情绪识别',
    'Understanding_of_movement_posture': '运动姿势理解',
    'Good_recognition_and_understanding_of_games': '游戏精彩识别理解',
    'Google_Search_by_Image': '以图搜图',
    'Musical_emotion_gene_recognition': '音乐情感基因识别',
    'Face_recognition_of_immico': '咪咕人脸识别',
    'under_mov_post': '运动姿势理解',
    'gameai_re': '游戏精彩识别理解',
    'search_by_image': '以图搜图',
    'CPU_average': 'CPU平均使用率',
    'MEM_average': '内存平均使用率',
    'gpu_average': 'GPU平均使用率'
}
url = 'http://172.20.15.2:8081/ums/business/json/'
def sendjson(url,data):
    headers = {'Content-Type': 'application/json'}
    request = urllib2.Request(url=url, headers=headers, data=json.dumps(data))
    response = urllib2.urlopen(request)
    print(response.read())
def analysis_gpu(filename):
    logfile=filename
    str1='''
    DATE:20180804  gpu_average:0.02
    DATE:20180805  gpu_average:0.02
    '''
    content_dict={}
    with open(logfile,'r') as f:
        for line in f:
            loglist=line.split()
            # print(loglist)
            key=loglist[0].strip().split(':')[1]
            gpu_average=loglist[1].strip().split(':')[1]
            content_dict[key]=[gpu_average]
    # print(content_dict)
    return content_dict
def sendgpu(content_dict):
    current_time_min = time.strftime('%Y%m%d%H%M')
    current_time_sec = time.strftime('%Y%m%d%H%M') + "00"
    SerialNumber=401
    business_name_gpu = name_dict['gpu_average']
    business_name_gpu = '成都主节点'
    JCZB_Code_gpu = '302301612200'
    for business_date in content_dict:
        business_day=business_date+"2359"
        info_list = content_dict[business_date]
        gpu_average=info_list[0]
        data = {"SysCode":"3023",
        "Time":current_time_sec,
        "SerialNumber":SerialNumber,
        "Data":[
        {"JCZB_Code":JCZB_Code_gpu,
        "Aggregate_Flag":0,
        "Log_Time":current_time_min,
        "JCZB_Dim_Value":"10:"+business_day+";200:"+business_name_gpu,
        "JCZB_Value":gpu_average}
        ]
        }
        print(data)
        # sendjson(url,data)
content_dict=analysis_gpu('gpu.log')
sendgpu(content_dict)



