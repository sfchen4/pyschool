#!/usr/bin/env python2.7
# coding:utf-8
#capacity_statistics.py
# import urllib2
import json
import time
import sys
import os

name_dict = {
    'musicGene_re': '音乐情感基因识别',
    'car_re': '点播汽车识别',
    'adv_re': '直播广告识别',
    'star_re': '点播明星识别',
    'car_star_adv_re': '明星汽车广告识别',
    'Direct_seeding_emotion_recognition': '直播情绪识别',
    'Understanding_of_movement_posture': '运动姿势理解',
    'Good_recognition_and_understanding_of_games': '游戏精彩识别理解',
    'Google_Search_by_Image': '以图搜图',
    'Musical_emotion_gene_recognition': '音乐情感基因识别',
    'Face_recognition_of_immico': '咪咕人脸识别',
    'under_mov_post': '运动姿势理解',
    'gameai_re': '游戏精彩识别理解',
    'search_by_image': '以图搜图',
    'CPU_average': 'CPU平均使用率',
    'MEM_average': '内存平均使用率'
}
def getargs():
    if len(sys.argv) < 3:
        print("3 args must be give,you have gived %s args" %len(sys.argv))
        exit()
    filename=sys.argv[1]
    jsontype=sys.argv[2]
    if not os.path.isfile(filename):
        print("the file %s does not exists!" %filename)
        exit()
    else:
        if jsontype not in ['cpumem', 'capnum']:
            print("the type value must be in [cpumem,capnum]")
            exit()
        else:
            return [filename,jsontype]
def sendjson(url,data):
    headers = {'Content-Type': 'application/json'}
    request = urllib2.Request(url=url, headers=headers, data=json.dumps(data))
    response = urllib2.urlopen(request)
    print(response.read())

def analysis_cap(filename):
    content_dict={}
    with open(filename,'r') as f:
        for line in f:
            key,name,num_cal,suc_rate=line.split('|')
            # key=line_list[0].strip()
            # name=line_list[1].strip()
            # num_cal=line_list[2].strip()
            # suc_rate=line_list[3].strip()
            print(key,name,num_cal,suc_rate)
            if key not in content_dict:
                content_dict[key]=[]
            inner_dic={}
            inner_dic['name']=name
            inner_dic['num_cal']=int(num_cal)
            inner_dic['suc_rate']=float(suc_rate)
            content_dict[key].append(inner_dic)
    return content_dict

def send_cap_stat(content_dict):
    current_time_min = time.strftime('%Y%m%d%H%M')
    current_time_sec = time.strftime('%Y%m%d%H%M') + "00"
    for business_date in content_dict:
        business_day=business_date+"2359"
        for info_dic in content_dict[business_date]:
            en_name=info_dic['name']
            business_name=name_dict[en_name]
            # print(business_name)
            number_of_calls = info_dic['num_cal']
            print(type(number_of_calls))
            Success_rate=info_dic['suc_rate']
            print(type(Success_rate))
            JCZB_Type='302301MIGUAI01000001'
            data = {"SysCode":"3023",
            "Time":current_time_sec,
            "SerialNumber":401,
            "Data":[
            {"JCZB_Code":JCZB_Type,
            "Aggregate_Flag":0,
            "Log_Time":current_time_min,
            "JCZB_Dim_Value":"10:"+business_day+";70:"+business_name,
            "JCZB_Value":number_of_calls}
            ]
            }
            # headers = {'Content-Type': 'application/json'}
            # request = urllib2.Request(url=url, headers=headers, data=json.dumps(data))
            # response = urllib2.urlopen(request)
            # print(response)

res=analysis_cap('sum.txt')
send_cap_stat(res)