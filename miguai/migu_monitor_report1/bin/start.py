import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import config
from core import main

if __name__ == '__main__':
    url = config.url
    # file_name=sendjson.getargs()[0]
    # type_name=sendjson.getargs()[1]
    file_name='backup/sum.txt'
    type_name='capnum'
    if type_name == 'cpumem':
        content_dict=main.analysis_cpumem(file_name)
        main.sendcpumem(content_dict,url,config.name_dict)
    elif type_name == 'cap':
        content_dict=main.analysis_cap(file_name)
        main.send_cap(content_dict,url,config.name_dict)
    elif type_name == 'gpu':
        content_dict=main.analysis_gpu(file_name)
        main.sendgpu(content_dict,url,config.node_name)
    elif type_name == 'weblogic':
        pass