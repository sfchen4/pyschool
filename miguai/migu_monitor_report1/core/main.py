#!/usr/bin/env python2.7
# coding:utf-8
#main.py
#
import urllib2
import json
import time
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import config

def getargs():
    if len(sys.argv) < 3:
        print("3 args must be give,you have gived %s args" %len(sys.argv))
        exit()
    filename=sys.argv[1]
    type_name=sys.argv[2]
    if not os.path.isfile(filename):
        print("the file %s does not exists!" %filename)
        exit()
    else:
        if type_name not in ['cpumem', 'capnum','gpu','weblogic']:
            print("the type value must be in [cpumem,capnum,gpu]")
            exit()
        else:
            return [filename,type_name]

def sendjson(url,data):
    headers = {'Content-Type': 'application/json'}
    request = urllib2.Request(url=url, headers=headers, data=json.dumps(data))
    response = urllib2.urlopen(request)
    print(response.read())

def analysis_cap(filename):
    content_dict={}
    with open(filename,'r') as f:
        for line in f:
            key,name,num_cal,suc_rate=line.split('|')
            if key.strip() not in content_dict:
                content_dict[key.strip()]=[]
            inner_dic={
                'name':name.strip(),
                'num_cal':int(num_cal.strip()),
                'suc_rate':float(suc_rate.strip())
            }

            content_dict[key.strip()].append(inner_dic)
    return content_dict

def analysis_cpumem(logfile):
    content_dict={}
    with open(logfile,'r') as f:
        for line in f:
            loglist=line.split()

            key=loglist[0].strip().split(':')[1]
            CPU_average=loglist[1].strip().split(':')[1]
            MEM_average=loglist[2].strip().split(':')[1]
            content_dict[key]=[CPU_average, MEM_average]
    return content_dict

def analysis_gpu(logfile):
    content_dict={}
    with open(logfile,'r') as f:
        for line in f:
            loglist=line.split()

            key=loglist[0].strip().split(':')[1]
            gpu_average=loglist[1].strip().split(':')[1]
            content_dict[key]=[gpu_average]
    return content_dict



def send_cap(content_dict,url,name_dict):
    cur_time = time.strftime('%Y%m%d%H%M%S')
    # define data vars
    jczb_code_num = "302301610000"
    jczb_code_rate = "302301610300"
    aggregate_flag = 0
    cnlogo_num = 184
    cnlogo_rate = 185

    for business_day in content_dict:
        business_time=business_day+"2359"
        for info_dic in content_dict[business_day]:
            cap_name=info_dic['name']
            number_of_calls = info_dic['num_cal']
            success_rate=info_dic['suc_rate']
            business_name = config.name_dict[cap_name]
            jczb_dim_value_num="10:%s;%s:%s" %(business_time,cnlogo_num,business_name)
            jczb_dim_value_rate="10:%s;%s:%s" %(business_time,cnlogo_rate,business_name)

            data={"SysCode":"3023","Time":config.Time,"SerialNumber":config.serialnumber_cap,
                "Data":[
                    {"JCZB_Code":config.code_capnoc,"Aggregate_Flag":0,"Log_Time":config.Log_Time,"JCZB_Dim_Value":jczb_dim_value_num,"JCZB_Value":number_of_calls},
                    {"JCZB_Code":config.code_caprat,"Aggregate_Flag":0,"Log_Time":config.Log_Time,"JCZB_Dim_Value":jczb_dim_value_rate,"JCZB_Value":success_rate}
            ]}
            sendjson(url, data)
def sendcpumem(content_dict,url,name_dict):
    current_time_min = time.strftime('%Y%m%d%H%M')
    current_time_sec = time.strftime('%Y%m%d%H%M') + "00"
    SerialNumber=110
    business_name_cpu = name_dict['CPU_average']
    business_name_cpu = '咪咕音乐'
    business_name_mem = name_dict['MEM_average']
    JCZB_Code_cpu = '302301612000'
    JCZB_Code_mem = '302301612100'

    for business_date in content_dict:
        business_day=business_date+"2359"
        info_list = content_dict[business_date]
        CPU_average=info_list[0]
        MEM_average=info_list[1]

        data = {"SysCode":"3023",
        "Time":current_time_sec,
        "SerialNumber":config.serialnumber_cpumem,
        "Data":[
        {"JCZB_Code":JCZB_Code_cpu,
        "Aggregate_Flag":0,
        "Log_Time":current_time_min,
        "JCZB_Dim_Value":"10:"+business_day+";200:"+business_name_cpu,
        "JCZB_Value":CPU_average}
        ,
        {"JCZB_Code":JCZB_Code_mem,
        "Aggregate_Flag":0,
        "Log_Time":current_time_min,
        "JCZB_Dim_Value":"10:"+business_day+";70:"+business_name_mem,
        "JCZB_Value":MEM_average}
        ]
        }

        sendjson(url,data)

def sendgpu(content_dict,url,node_name):
    current_time_min = time.strftime('%Y%m%d%H%M')
    current_time_sec = time.strftime('%Y%m%d%H%M') + "00"
    SerialNumber=401
    # business_name_gpu = name_dict['gpu_average']
    business_name_gpu = node_name
    JCZB_Code_gpu = '302301612200'
    for business_date in content_dict:
        business_day=business_date+"2359"
        info_list = content_dict[business_date]
        gpu_average=info_list[0]
        data = {"SysCode":"3023",
        "Time":current_time_sec,
        "SerialNumber":config.serialnumber_gpu,
        "Data":[
        {"JCZB_Code":JCZB_Code_gpu,
        "Aggregate_Flag":0,
        "Log_Time":current_time_min,
        "JCZB_Dim_Value":"10:"+business_day+";200:"+business_name_gpu,
        "JCZB_Value":gpu_average}
        ]
        }
        print(data)
        sendjson(url,data)